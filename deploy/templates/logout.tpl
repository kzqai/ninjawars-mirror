{literal}
<style>
.ad-section{
	max-width:900px;margin-left:auto;margin-right:auto;
}
</style>
{/literal}
<h1>
	You have been logged out.
</h1>
<section class='glassbox centered thick'>
<p class='lead'>
	Sorry to see you go!  Come back soon!
	You can <a href='/'>return to the homepage</a> or <a href='/login.php'>log in again here.</a>
</p>
</section>
<section class='ad-section glassbox'>
<p>
Want to learn more about ninjas while you're gone? Maybe these books will be of interest to you.
</p>
<div class='thick clearfix'>

	<div class='half-column'>
		<p>A short, illustrated take on some of the historical facts about ninja by Historian Stephen Turnbull.  Part of a series on historical warriors.
		</p>
		<a href="http://www.amazon.com/gp/product/1841765252/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=1841765252&linkCode=as2&tag=programmingpi-20&linkId=UFTJMPAR5NZUR4IG">Ninja AD 1460-1650 (Warrior)</a><img src="http://ir-na.amazon-adsystem.com/e/ir?t=programmingpi-20&l=as2&o=1&a=1841765252" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
	</div>
	<div class='half-column'>
		<p>The Illustrated Ninja Handbook is focused more on applying Ninja techniques and thought processes to modern martial arts, with some of the history and old ninja weapons thrown in.
		</p>
		<a href="http://www.amazon.com/gp/product/4805313056/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=4805313056&linkCode=as2&tag=programmingpi-20&linkId=EBOFX6H7HJLQHTWA">The Illustrated Ninja Handbook: Hidden Techniques of Ninjutsu</a><img src="http://ir-na.amazon-adsystem.com/e/ir?t=programmingpi-20&l=as2&o=1&a=4805313056" width="1" height="1" border="0" alt="" style="border:none !important; margin:0px !important;" />
	</div>
	<div class='inline-block glassbox'>
	<SCRIPT charset="utf-8" type="text/javascript" src="http://ws-na.amazon-adsystem.com/widgets/q?rt=ss_ssw&ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fprogrammingpi-20%2F8003%2F0e21130c-3468-4f24-bbd7-acaeb7142afc&Operation=GetScriptTemplate"> </SCRIPT> <NOSCRIPT><A HREF="http://ws-na.amazon-adsystem.com/widgets/q?rt=ss_ssw&ServiceVersion=20070822&MarketPlace=US&ID=V20070822%2FUS%2Fprogrammingpi-20%2F8003%2F0e21130c-3468-4f24-bbd7-acaeb7142afc&Operation=NoScript">Amazon.com Widgets</A></NOSCRIPT>
	</div>
	<div class='google-ad inline-block glassbox'>
	    <!-- Google Ad -->
    <script type="text/javascript"><!--
    google_ad_client = "pub-9488510237149880";
    /* 300x250, created 12/17/09 */
    google_ad_slot = "9563671390";
    google_ad_width = 300;
    google_ad_height = 250;
    //-->
    </script>
    <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
    </script>
	</div>
</div>
<div class='centered thick'>


</div>
</section>

<footer class='glassbox'>
	<a href='/' class='return-to-location'>Return to the homepage</a>
</footer>