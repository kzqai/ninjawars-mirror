<h1>News Board</h1>
<style type='text/css'>
{literal}
.success-notice{
	border:green .3em solid;
	padding:.2em;
	margin: .3em auto;
}
.search_title {
  margin-bottom:20px;
  font-style:italic;
  font-size:smaller;
  color: gray;
}
.link-as-button{
  margin-bottom:.5em;margin-top:1em;float:right;margin-right:1.5em;
}
.news h3{
  background:#2D2D2D;
  color:#FD4326;
  margin-bottom:1em;
}
.news h3:before, .news h3:after{
  content:"—";
}
#news-list article{
  display:block;
  background-color:#333;
  font-size:larger;
  width:60%;
  min-width:30em;
  margin:0 auto;
}
#news-list article .post-content{
  padding:0 5%;
  width:90%;
}
#news-list article .post-content:first-child{
  padding-top:1em;
}
#news-list article + article{
  margin-top:3em;
}
#news-list article footer{
  color:gray;
  font-size:smaller;
  width:90%;
  padding:1em 5% .5em;
}
.news time{
  font-style:italic;
  color:grey;
}
{/literal}
</style>

<div id='news-list'>
{if isset($new_successful_submit) and $new_successful_submit}
  <div class='success-notice'>
    <strong>Your news successfully posted!</strong>
  </div>
{/if}
  
<a class='link-as-button' href="news.php">Refresh</a>
{if is_logged_in()}
<a class='link-as-button' href="news.php?new=true">New Post</a>
{/if}

{if isset($search_title)}
<p class="search_title">{$search_title}</p>
{/if}

{foreach from=$all_news key=index_news item=single_news}
{assign var="news_account" value=$single_news->getAccountss()}
<article class="news">
  {if $single_news->getTitle()}
  <h3>{$single_news->getTitle()|escape}</h3>
  {/if}
  <section class='post-content'>{$single_news->getContent()|escape}</section>
  <footer>
    - <span class='tags'> {$single_news->getTags()|to_tags} </span>
    {if $single_news->getCreated()} <time class='timeago' datetime='{$single_news->getCreated()}' title='{$single_news->getCreated()|date_format:"%A, %B %e, %Y"}'>{$single_news->getCreated()|date_format:"%A, %B %e, %Y"}</time>
    {else}
    <time class='timeago' datetime='{$smarty.now}' title='{$smarty.now|date_format:"%A, %B %e, %Y"}'>{$smarty.now|date_format:"%A, %B %e, %Y"}</time>
    {/if} by <a target="main" href="player.php?player_id={$news_account->getFirst()|to_playerid}">{$news_account->getFirst()|to_playername|escape}</a>
  </footer>
</article>
{/foreach}



</div><!-- End of news-list -->
